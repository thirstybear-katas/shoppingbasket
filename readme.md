# Shopping Basket kata

You are writing a simple shopping basket. The basket has the following contract:

* Items can be added to it. An item consists of an item name, and price.
* It can provide the total value of items currently held in the basket.

_Note: your Product Owner may add new requirements as development progresses_ 


